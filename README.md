# C# Xbox Controller Program

![XBox-Contoller-Testprogramm.PNG](https://pc-projekte.lima-city.de/media/images/xbox-contoller-testprogramm.png)

## EN
### Simple C# program examples for controlling an XBox controller in a classic C# Windows form application.

* XNA-Framework 4.0 (obsolete, since no longer supported by Microsoft)
* SharpDX as an alternative to the XNA framework

The program examples are subprojects of the project "alternative digital model railway control"see 
www.pc-projekte.de

The project is available as ZipFile. See also Info "Git Project Management".


## DE
### Einfache C# Programmbeispiele zur Ansteuerung eines XBox-Controllers in einer klassischen C#-Windows-Form-Anwendung.

* XNA-Framework 4.0 (veraltet, da nicht mehr von Microsoft unterstützt)
* SharpDX als Alternative zum XNA-Framework

Die Programmbeispiele sind Teilprojekte des Projektes "alternative digitale Modellbahnsteuerung", siehe
www.pc-projekte.de

Das Projekt steht als ZipFile zur Verfügung. Siehe auch Info "Git Projekt Management".

-------------------------------

#### very simple code connection


 *if ( (state.Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.A) == SharpDX.XInput.GamepadButtonFlags.A)*
            *{ // your code }*
            *else { // your code }*

 *if ( (state.Gamepad.Buttons & SharpDX.XInput.GamepadButtonFlags.X) == SharpDX.XInput.GamepadButtonFlags.X)*
            *{  // your code }*
            *else { //xour Code }*
